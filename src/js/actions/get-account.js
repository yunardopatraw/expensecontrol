import * as ynab from 'ynab';
import WebSettings from '../../../config/web/facade/web-settings';
import CommonFunction from '../../../common/facade/common-function';

export function getAccount() {

    const accessToken = WebSettings.ynap_access_token
    const ynabAPI = new ynab.API(accessToken);

    //console.log(theState);
    return (dispatch) => {
        dispatch(CommonFunction.dispatchData('loading', null, 'GET_ACCOUNT'))

        try {
            return ynabAPI.budgets.getBudgets()
                .then((budgetsResponse) => {
                    if (budgetsResponse.data.budgets && budgetsResponse.data.budgets.length > 0) {

                        var budget = budgetsResponse.data.budgets[0]
                        return ynabAPI.accounts.getAccounts(budget.id)
                            .then((accountResponse) => {
                                //console.log('accountResponse', accountResponse.data);
                                return dispatch(CommonFunction.dispatchData('finish', accountResponse.data, 'GET_ACCOUNT'))
                            })
                    }
                }).catch(err => {
                    return dispatch(CommonFunction.dispatchData('error', err, 'GET_ACCOUNT'))
                })
        } catch (error) {
            return dispatch(CommonFunction.dispatchData('error', error, 'GET_ACCOUNT'))
        }
    }
}