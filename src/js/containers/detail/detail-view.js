import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getTransaction } from '../../actions/get-transaction';
import { createTransaction } from '../../actions/create-transaction';

class DetailView extends Component {

    componentDidMount() {
        this.props.getTransaction();
    }

    renderAllBudgets() {

        //console.log('this.props.getTransaction', this.props.transaction);
        if (!this.props.transaction || this.props.transaction.status == 'loading') {
            return (
                <div>Expenses loading</div>
            )
        } else {
            if (this.props.transaction.data && this.props.transaction.data.transactions && this.props.transaction.status == 'finish') {

                var transactionList = this.props.transaction.data.transactions.filter(x => x.date == this.props.initdata.currentdate)

                return transactionList.map((transaction) => {

                    return (
                        <div key={transaction.id} className="budget-list">
                            <span>{transaction.date} | </span>
                            <span>{transaction.category_name} | </span>
                            <span>{transaction.amount} | </span>
                            <span>{transaction.memo} | </span>
                        </div>
                    )
                })
            }
        }
    }

    render() {

        var budgetName = (this.props.transaction && this.props.transaction.status == 'finish') ? this.props.transaction.data.name : "My Budget";

        return (
            <section className="overview">
                <a href="/" className="button">Back</a>
                <h1>{this.props.initdata.currentdate}</h1>
                <div>
                    <span>budget Name: {budgetName}</span>
                </div>
                <div className="budget-overview">
                    {this.renderAllBudgets()}
                </div>
            </section>
        )
    }
}
function mapStateToProps(state) {
    return {
        transaction: state.transaction,
        account: state.account
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTransaction: getTransaction,
        createTransaction: createTransaction,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailView);