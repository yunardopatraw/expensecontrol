import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getTransaction } from '../../actions/get-transaction';
import { createTransaction } from '../../actions/create-transaction';
import Chart from 'chart.js';
import moment from "moment";

class Overview extends Component {

    componentDidMount() {
        this.props.getTransaction();
    }

    renderAllBudgets() {

        //console.log('this.props.getTransaction', this.props.transaction);
        if (!this.props.transaction || this.props.transaction.status == 'loading') {
            return (
                <div>Expenses loading</div>
            )
        } else {
            if (this.props.transaction.data && this.props.transaction.data.transactions && this.props.transaction.status == 'finish') {
                return this.props.transaction.data.transactions.map((transaction) => {

                    return (
                        <div key={transaction.id} className="budget-list">
                            <span>{transaction.date} | </span>
                            <span>{transaction.category_name} | </span>
                            <span>{transaction.amount} | </span>
                            <span>{transaction.memo} | </span>
                        </div>
                    )
                })
            }
        }
    }

    checkStatus() {
        if (this.props.transaction && this.props.transaction.status == 'need-reload') {
            this.props.getTransaction();
        }
    }

    renderChart() {

        if (typeof window == 'undefined') {
            return
        }

        if (this.props.transaction && this.props.transaction.data && this.props.transaction.data.transactions) {

            //console.log('this.props.transaction.data', this.props.transaction.data);
            var xAxis = [];
            var yAxis = [];
            var currency = this.props.transaction.data.currency;
            for (let i = -14; i < 1; i++) {
                let theDate = moment().add(i, 'days').format('YYYY-MM-DD');

                xAxis.push(moment().add(i, 'days').format('DD MMM'));

                var totalAmmount = 0

                var recordsInDate = this.props.transaction.data.transactions.filter(x => x.date == theDate)

                //console.log('recordsInDate', recordsInDate);
                if (recordsInDate.length > 0) {
                    var totalAmmount = recordsInDate
                        .map(a => a.amount)
                        .reduce((a, b) => {
                            return a + b;
                        });
                }

                yAxis.push(Math.abs(totalAmmount / 1000));
            }

            var ctx = document.getElementById("expenses-chart");
            Chart.defaults.global.defaultFontFamily = "'Roboto Mono', monospace";
            var weatherChart = new Chart(ctx, {
                type: 'bar',
                showLine: false,

                data: {
                    labels: xAxis,
                    datasets: [{
                        data: yAxis,
                        backgroundColor: ['rgba(250, 173, 1, 0.2)'],
                        borderColor: ['rgba(250, 173, 1, 1)'],
                        borderWidth: 1,
                        pointBackgroundColor: 'rgba(56, 214, 197, 0.8)',
                        pointBorderColor: 'rgba(56, 214, 197, 1)',
                        pointRadius: 0,
                        pointHitRadius: 10,
                        pointHoverRadius: 5,
                    },]
                },
                options: {
                    tooltips: {
                        backgroundColor: 'rgba(56,214,197,0.9)',
                        footerFontStyle: 'bold',
                        bodyFontColor: '#fff',
                        callbacks: {
                            title: function (tooltipItem, data) {
                                return data.datasets[tooltipItem[0].datasetIndex].data[tooltipItem[0].index] + '(' + currency + ')';
                            },
                            label: function (tooltipItem, data) {
                                return '';
                            }
                        }

                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value, index, values) {
                                    return value + '(' + currency + ')';
                                },
                            },
                            gridLines: {
                                drawTicks: false,
                                zeroLineColor: 'rgba(0,0,0,0.02)',
                                borderDash: [1],
                                lineWidth: 0.6,

                            },
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false,
                            },

                        }]
                    }
                }
            });


        }



    }

    render() {

        this.checkStatus();
        var budgetName = (this.props.transaction && this.props.transaction.status == 'finish') ? this.props.transaction.data.name : "My Budget";
        this.renderChart();

        return (
            <section className="overview">

                {/*<div className="budget-overview">
                    {this.renderAllBudgets()}
                </div>*/}
                <div className="chart-container">
                    <div className="budget-label">
                        <span>{budgetName}</span>
                    </div>
                    <canvas id="expenses-chart" className="expenses-chart" ></canvas>
                </div>
            </section>
        )
    }
}
function mapStateToProps(state) {
    return {
        transaction: state.transaction,
        account: state.account
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTransaction: getTransaction,
        createTransaction: createTransaction,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Overview);