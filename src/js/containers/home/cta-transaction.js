import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getAccount } from '../../actions/get-account';
import { getCategory } from '../../actions/get-category';
import { createTransaction } from '../../actions/create-transaction';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class CtaTransaction extends Component {

    componentDidMount() {
        this.props.getAccount();
        this.props.getCategory();
    }

    constructor(props) {
        super(props);

        this.state = {
            categoryId: '',
            accountId: '',
            amountInput: 0,
            noteInput: '',
            dateInput: new Date(),
            errorMessage: '',
        };
    }

    renderAllBudgets() {

        //console.log('this.props.getTransaction', this.props.transaction);
        if (!this.props.transaction || this.props.transaction.status == 'loading') {
            return (
                <div>Expenses loading</div>
            )
        } else {
            if (this.props.transaction.data && this.props.transaction.data.transactions && this.props.transaction.status == 'finish') {
                return this.props.transaction.data.transactions.map((transaction) => {

                    return (
                        <div key={transaction.id}>
                            <span>{transaction.category_name}</span>
                            <span>{transaction.amount}</span>
                            <span>{transaction.memo}</span>
                        </div>
                    )
                })
            }
        }
    }

    createTransaction() {

        var message = ''
        // create some validation

        if (this.state.amountInput < 1){
            message = 'Amount is mandatory'
        }
        if (this.state.categoryId == ''){
            message = 'Category is mandatory'
        }
        if (this.state.accountId == ''){
            message = 'Account is mandatory'
        }

        if (message != ''){
            this.setState({
                errorMessage : message
            })

            return;
        }

        if (this.props.transaction.status == 'loading'){
            return;
        }


        if (this.props.transaction) {
            this.props.createTransaction(
                this.props.transaction.data.id,
                this.state.accountId,
                this.state.dateInput,
                this.state.amountInput,
                this.state.categoryId,
                this.state.noteInput);

            this.setState({
                categoryId: '',
                amountInput: 0,
                noteInput: '',
                errorMessage: '',
            })
        }
    }

    renderAccountItems() {
        if (this.props.account && this.props.account.status == 'finish' && this.props.account.data) {
            return this.props.account.data.accounts.map((account) => {

                return (
                    <option value={account.id} key={account.id}>{account.name}</option>
                )
            })
        }
    }

    renderCategoryGroup() {
        if (this.props.category && this.props.category.status == 'finish' && this.props.category.data) {
            return this.props.category.data.category_groups.map((group) => {

                return (
                    <optgroup label={group.name} key={group.id}>
                        {this.renderCategoryItem(group)}
                    </optgroup>

                )
            })
        }
    }

    renderCategoryItem(categoryGroup) {
        if (categoryGroup && categoryGroup.categories && categoryGroup.categories.length > 0) {
            return categoryGroup.categories.map((category) => {

                return (
                    <option value={category.id} key={category.id}>{category.name}</option>
                )
            })
        }
    }

    render() {

        var btnLabel = 'SUBMIT';
        if (this.props.transaction && this.props.transaction.status == 'loading'){
            btnLabel = 'LOADING...';
        }

        return (
            <section className="add-transaction">
                <h2>Create Transaction:</h2>
                <div className="create-transaction">
                    <div className="input account-section">
                        <span>Select Account</span>
                        <select value={this.state.accountId}
                            onChange={(evt) => this.setState({ accountId: evt.target.value })}>
                            <option value='' key='default'>== Please Select ==</option>
                            {this.renderAccountItems()}
                        </select>
                    </div>
                    <div className="input category-section">
                        <span>Select Category:</span>
                        <select value={this.state.categoryId}
                            onChange={(evt) => this.setState({ categoryId: evt.target.value })}>
                            <option value='' key='default'>== Please Select ==</option>
                            {this.renderCategoryGroup()}
                        </select>
                    </div>
                    <div className="input amount-section">
                        <span>Amount:</span>
                        <input type="number" id="amount-input" placeholder="Enter ammount" min="0" max="9999999"
                            value={this.state.amountType}
                            onChange={evt => this.setState({ amountInput: evt.target.value })}
                            autoComplete="false">
                        </input>
                    </div>
                    <div className="input note-section">
                        <span>Note:</span>
                        <input type="text" id="note-input" placeholder="Enter Note"
                            value={this.state.noteInput}
                            onChange={evt => this.setState({ noteInput: evt.target.value })}
                            autoComplete="false">
                        </input>
                    </div>
                    <div className="input date-section">
                        <span>Transaction date:</span>
                        <DatePicker selected={this.state.dateInput}
                            onChange={evt => this.setState({ dateInput: evt })}>
                        </DatePicker>
                    </div>


                    <div className="button" onClick={() => this.createTransaction()}>{btnLabel}</div>
                </div>
                <div className="error-msg">
                    <span>{this.state.errorMessage}</span>
                
                </div>
            </section>
        )
    }
}
function mapStateToProps(state) {
    return {
        transaction: state.transaction,
        account: state.account,
        category: state.category,
        createTransactionStatus: state.createTransactionStatus,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createTransaction: createTransaction,
        getAccount: getAccount,
        getCategory: getCategory,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CtaTransaction);