import React, {Component} from 'react';
import {connect} from 'react-redux';

class Header extends Component{

    render(){
       
        return(
            <header className="header">
                <div>EXPENSES CONTROL SYSTEM</div>
            </header>

        )

    }

}

function mapStateToProps(state) {
    return {
    }
}

export default connect(mapStateToProps)(Header);