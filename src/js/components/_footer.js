import React, {Component} from 'react';
import {connect} from 'react-redux';

class Footer extends Component{

    render(){
       
        return(
            <header className="footer">
                <div className="copyright">powered by someone</div>
            </header>

        )

    }

}

function mapStateToProps(state) {
    return {
    }
}

export default connect(mapStateToProps)(Footer);