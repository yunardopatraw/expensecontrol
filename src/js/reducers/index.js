import { combineReducers } from 'redux';
import AccountReducers from './ynab-response/get-account';
import CategoryReducers from './ynab-response/get-category';
import TransactionReducers from './ynab-response/get-transaction';
import CreateTransactionReducers from './ynab-response/create-transaction';

const allReducers = combineReducers({
    account: AccountReducers,
    category: CategoryReducers,
    transaction: TransactionReducers,
    createTransaction: CreateTransactionReducers,
})

export default allReducers;