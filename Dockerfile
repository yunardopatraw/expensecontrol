#FROM node:alpine as builder
FROM node:10.16.3-alpine

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm config set registry http://registry.npmjs.org/ && npm install

# Bundle app source
COPY . /usr/src/app
RUN npm run build
#RUN npm run export

EXPOSE 3000
CMD [ "npm", "run", "start-serve" ]
