import React from 'react';
import {withRouter} from 'next/router';
import {Provider} from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import allReducers from '../src/js/reducers/index';
import Layout from '../src/js/components/_layout';
import Overview from '../src/js/containers/home/overview';
import CtaTransaction from '../src/js/containers/home/cta-transaction';
import ListingTransaction from '../src/js/containers/home/listing-transaction';

const middleware = applyMiddleware(logger, thunk);
const store = createStore(allReducers, middleware);

const Home = withRouter((props) => {
    return (
        <Provider store={store}>
            <Layout initdata={props.initdata}>
                <Overview></Overview>
                <CtaTransaction></CtaTransaction>
                <ListingTransaction></ListingTransaction>
            </Layout>
        </Provider>
    )
});

Home.getInitialProps = async function(context) {
    
    var data = {};
    data.language = context.query.lang ? context.query.lang : 'en'
    data.title = "Expense Control"

	return {
		initdata: data
	}
}

export default Home;